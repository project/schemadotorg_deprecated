Table of contents
-----------------

* Introduction
* Features
* Requirements
* Usage
* References
* Todo


Introduction
------------

The **Schema.org Blueprints Action** provides support for 
<https:://schema.org/Action> using a call to action (CTA) paragraph type.


Features
--------

- Creates a 'Call to action' paragraph type which is mapped to
  [Webpage](https://schema.org/WebPage).
- Sets JSON-LD <https://schema.org/potentialAction> property for links using 
  the custom 'schema\_potential\_action' link attribute.


Requirements
------------

**[Link Attributes](https://www.drupal.org/project/link_attributes)**  
Provides a widget to allow settings of link attributes for menu links.


Usage
-----

- Enable the Schema.org Blueprints Layout Paragraphs module
- Insert CTA into a layout.


References
----------

- [Schema.org/Actions](https://schema.org/Action)
- [Schema.org/ConsumeActions](https://schema.org/ConsumeAction)
- [Schema.org Actions](https://www.seroundtable.com/schema-actions-18438.html)
- [Actions in schema.org](https://www.w3.org/wiki/images/2/25/Schemaorg-actions-draft5.pdf)
- [What is an “Action” in Schema?](https://ondyr.com/what-is-action-schema/)
- [How to add article schema markup to blog posts](https://www.hallaminternet.com/introducing-schema-org-action-markups/?amp)


Todo
----

- Replace custom 'class' field with
  [Paragraphs Collection](https://www.drupal.org/project/paragraphs_collection) module.
  @see schemadotorg\_layout\_paragraphs\_paragraph\_view\_alter()

- Determine if an how to leverage Schema.org Actions via Email.
  @see https://developers.google.com/gmail/markup/overview
