Table of contents
-----------------

* [Introduction](#introduction)
* [Sub-modules](#sub-modules)
* [Installation](#installation)


Introduction
------------

Deprecated integrations for the [Schema.org Blueprints module](https://www.drupal.org/project/schemadotorg)
that are no longer supported.

> Where old Schema.org Blueprints sub-modules go to die.


Sub-modules
-----------

Below are the deprecated sub-modules include in this project

- **[Schema.org Blueprints Action](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_action)**  
  Provides support for https:://schema.org/Action using a call to action paragraph type.

- **[Schema.org Blueprints Flex Field](https://git.drupalcode.org/project/schemadotorg_deprecated/-/tree/1.0.x/modules/schemadotorg_flexfield)**  
  Allows a Flex field to be used to create Schema.org relationships within an entity type/bundle Schema.org mapping.



Installation
------------

Use the included [composer.libraries.json](https://git.drupalcode.org/project/schemadotorg_experimental/-/blob/1.0.x/composer.libraries.json) 
file to quickly install all sub-module dependencies.

As your Schema.org Blueprints project evolves, you may want to copy and adjust
the dependencies from the composer.libraries.json file into your project's
root composer.json.

[Watch how to install and set up the Schema.org Blueprints module](https://www.youtube.com/watch?v=Dludw8Eomh4)
